<?php
/**
 * Class object_Artist
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `artist`.
 * Validation rules should be modified in the database.
 * Generated on 2013-08-05 @ 23:12
 */
class Application_Model_Artist extends Application_Model_Object_Artist {

	/**
	 * These Values are used in the table `artist`
	 * 
	 * @var Array
	 */
	protected $_valid_properties = array('id', 'name', );

	public function __constructor($id=0, $column='id') {
		parent::__construct($id,$column);
		//Code;
	}

	public function save($saveRelations=false) {
		parent::save();
		//custom save;
	}

}