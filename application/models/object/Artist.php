<?php
/**
 * Class object_Artist
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `artist`.
 * Validation rules should be modified in the database.
 * Generated on 2013-08-05 @ 23:12
 */
abstract class Application_Model_Object_Artist extends Kaaz_Model {

	/**
	 * The database table used
	 * 
	 * @var String
	 */
	protected $_tablename = 'artist';

	/**
	 * @var int(11)
	 */
	protected $_name;

	protected function _set_name($val) {
		$this->_name =  $val;
	}

}