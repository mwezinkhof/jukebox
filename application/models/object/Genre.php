<?php
/**
 * Class object_Genre
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `genre`.
 * Validation rules should be modified in the database.
 * Generated on 2013-08-05 @ 23:12
 */
abstract class Application_Model_Object_Genre extends Kaaz_Model {

	/**
	 * The database table used
	 * 
	 * @var String
	 */
	protected $_tablename = 'genre';

	/**
	 * @var int(11)
	 */
	protected $_name;

	protected function _set_name($val) {
		$this->_name =  $val;
	}

}