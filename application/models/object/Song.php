<?php
/**
 * Class object_Song
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `song`.
 * Validation rules should be modified in the database.
 * Generated on 2013-08-06 @ 00:57
 */
abstract class Application_Model_Object_Song extends Kaaz_Model {

	/**
	 * The database table used
	 * 
	 * @var String
	 */
	protected $_tablename = 'song';

	/**
	 * @var int(11)
	 */
	protected $_genre_id;

	/**
	 * @var varchar(100)
	 */
	protected $_title;

	/**
	 * @var int(11)
	 */
	protected $_artist_id;

	/**
	 * @var int(11)
	 */
	protected $_year;

	/**
	 * @var varchar(100)
	 */
	protected $_file;

	protected function _set_genre_id($val) {
		$this->_genre_id = (int) $val;
	}

	protected function _set_title($val) {
		$this->_title = substr($val,0, 100);
	}

	protected function _set_artist_id($val) {
		$this->_artist_id = (int) $val;
	}

	protected function _set_year($val) {
		$this->_year = (int) $val;
	}

	protected function _set_file($val) {
		$this->_file = substr($val,0, 100);
	}

}