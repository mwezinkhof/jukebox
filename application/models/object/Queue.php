<?php
/**
 * Class object_Queue
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `queue`.
 * Validation rules should be modified in the database.
 * Generated on 2013-08-06 @ 10:35
 */
abstract class Application_Model_Object_Queue extends Kaaz_Model {

	/**
	 * The database table used
	 * 
	 * @var String
	 */
	protected $_tablename = 'queue';

	/**
	 * @var int(11)
	 */
	protected $_song_id;

	/**
	 * @var datetime
	 */
	protected $_request_time;

	/**
	 * @var datetime
	 */
	protected $_played_time;

	/**
	 * @var tinyint(1)
	 */
	protected $_in_queue;

	protected function _set_song_id($val) {
		$this->_song_id = (int) $val;
	}

	protected function _set_request_time($val) {
		$this->_request_time = $val;
	}

	protected function _set_played_time($val) {
		$this->_played_time = $val;
	}

	protected function _set_in_queue($val) {
		$this->_in_queue = (bool) $val;
	}

}