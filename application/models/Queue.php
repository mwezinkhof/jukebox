<?php
/**
 * Class object_Queue
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `queue`.
 * Validation rules should be modified in the database.
 * Generated on 2013-08-06 @ 10:35
 */
class Application_Model_Queue extends Application_Model_Object_Queue {

	/**
	 * These Values are used in the table `queue`
	 * 
	 * @var Array
	 */
	protected $_valid_properties = array('id', 'song_id', 'request_time', 'played_time', 'in_queue', );

	public function __constructor($id=0, $column='id') {
		parent::__construct($id,$column);
		//Code;
	}

	public function save($saveRelations=false) {
		parent::save();
		//custom save;
	}

}