<?php
/**
 * Class object_Song
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `song`.
 * Validation rules should be modified in the database.
 * Generated on 2013-08-05 @ 23:12
 */
class Application_Model_Song extends Application_Model_Object_Song {

	/**
	 * These Values are used in the table `song`
	 * 
	 * @var Array
	 */
	protected $_valid_properties = array('id', 'genre_id', 'title', 'artist_id', 'year', 'file');

	public function __constructor($id=0, $column='id') {
		parent::__construct($id,$column);
		//Code;
	}

	public function save($saveRelations=false) {
		parent::save();
		//custom save;
	}

}