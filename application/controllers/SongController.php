<?php

class SongController extends Kaaz_Controller{
    public static $SONG_PATH = '/music/';
    public function init(){
        parent::init();
        /* Initialize action controller here */
    }

    public function indexAction(){
		//Nieuws ofzo
    }
	public function listAction(){
		if($this->_jsonrequest){
                    
			$queue = new Application_Model_Queue();

			$q = $queue->getdb()->query(
				'SELECT s.id,s.title,a.name as name,g.name as genre 
				FROM song s 
				LEFT JOIN artist a on a.id = s.artist_id
				LEFT JOIN genre g on g.id = s.genre_id
				ORDER BY title asc'
			);
			
			$this->getHelper('json')->sendJson( $q->fetchAll(), true);
		}
		echo 'Not today.';
		die();
	}
	public function nowplayingAction(){
		
		$ip = "213.148.244.102"; 
		$port = "8000"; 
		$ppage='';
		$cur_song='';
		$fp = fsockopen($ip, $port, $errno, $errstr, 1); 
		if(!$fp){ 
		} 
		else{
			fputs($fp,"GET /status2.xsl HTTP/1.0\r\nUser-Agent: Icecast2 XSL Parser (Mozilla Compatible)\r\n\r\n"); 
			while(!feof($fp)) {$ppage .= fgets($fp, 1000);} 
			fclose($fp); 
			$ppage = @ereg_replace(".*<pre>", "", $ppage); 
			$ppage = @ereg_replace("</pre>.*", ",", $ppage); 
			$numbers = explode(",",$ppage); 
			$cur_song = $numbers[16];
		} 
		die($cur_song);
	}
    public function uploadAction(){
        $ret = array();
        $ret['success'] = false;
		$msg= '';
        $file_name = SongController::$SONG_PATH . time().'.mp3';
        if(isset($_FILES['song_file'])){
            if(strtolower(substr($_FILES['song_file']['name'],-4)) ==  '.mp3'){
               $x = new Id3v2();
               $fi = ($x->read($_FILES['song_file']['tmp_name']));
               if($fi !== false){                         
                   if(!isset($fi['Title'])){
                             $fi['Title'] = $_FILES['song_file']['name'];
                    }
                    $ar = new Application_Model_Artist(array('name'=>''.@$fi['Artist']));
                    $ar->name = @$fi['Artist'];
                    $ar->save();
                    $gen = new Application_Model_Genre(array('name'=>''.@$fi['Genre']));
                    $gen->name=@$fi['Genre'];
                    $gen->save();
                    $song = new Application_Model_Song(array('title'=>''.@$fi['Title'],
                                                             'genre_id'=>$gen->id,
                                                             'artist_id'=>$ar->id,
                                                             'year'=>intval(@$fi['Year'])));
                    if($song->id == 0){
                        if(move_uploaded_file($_FILES['song_file']['tmp_name'],
                                             $file_name)){



						$song->title=@$fi['Title'];
						$song->genre_id=$gen->id;
						$song->artist_id=$ar->id;
						$song->year=@$fi['Year'];
						$song->file = $file_name;
						$song->save();
						$msg.="Upload success:<br/>";
						$msg.="Title: ".$song->title;
						$msg.="<br/>Artist: ".$ar->name;
						$msg.="<br/>Genre: ".$gen->name;
						$msg.="<br/>Year: ".$song->year;
						$ret['success']=true;
                        }
                    
                    }
               }
            }
        }
		$ret['message'] = $msg;
        $this->getHelper('json')->sendJson($ret,true);
        
    }
}
class Id3v2{	
    public $error;

    private $tags = array(
            'TALB' => 'Album',
            'TCON' => 'Genre',
            'TENC' => 'Encoder',
            'TIT2' => 'Title',
            'TPE1' => 'Artist',
            'TPE2' => 'Ensemble',
            'TYER' => 'Year',
            'TCOM' => 'Composer',
            'TCOP' => 'Copyright',
            'TRCK' => 'Track',
            'WXXX' => 'URL',
            'COMM' => 'Comment'
            );
		 
private function decTag($tag, $type){
        //TODO- handling of comments is quite weird
        //but I don't know how it is encoded so I will leave the way it is for now
    if ($type == 'COMM')
        {
                $tag = substr($tag, 0, 3) . substr($tag, 10);
        }
        //mb_convert_encoding is corrupted in some versions of PHP so I use iconv
        switch (ord($tag[2]))
        {
                case 0: //ISO-8859-1
                                return iconv('UTF-8', 'ISO-8859-1', substr($tag, 3));
                case 1: //UTF-16 BOM
                                return iconv('UTF-16LE', 'UTF-8', substr($tag, 5));
                case 2: //UTF-16BE
                                return iconv('UTF-16BE', 'UTF-8', substr($tag, 5));
                case 3: //UTF-8
                                return substr($tag, 3);
        }
        return false;
}

public function read($file)
{
        $f = fopen($file, 'r');
        $header = fread($f, 10);
        $header = @unpack("a3signature/c1version_major/c1version_minor/c1flags/Nsize", $header);
if (!$header['signature'] == 'ID3'){
                $this->error = 'This file does not contain ID3 v2 tag';		
                fclose($f);
                return false;		
        }

        $result = array();
        for ($i=0; $i<22; $i++)
        {
                $tag = rtrim(fread($f, 6));

                if (!isset($this->tags[$tag])) break;

                $size = fread($f, 2);
                $size = @unpack('n', $size);
                $size = $size[1]+2;

                $value = fread($f, $size);	
                $value = $this->decTag($value, $tag);

                $result[$this->tags[$tag]] = $value;
        }

        fclose($f);
        return $result;	
	}	
}
