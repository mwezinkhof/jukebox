<?php

class QueueController extends Kaaz_Controller{
    public function init(){
        parent::init();
        /* Initialize action controller here */
    }
	public function addsongAction(){
		if($this->_jsonrequest){
			$song = new Application_Model_Song($this->getRequest()->getParam('song_id'));
			if($song->id > 0){
				$q = new Application_Model_Queue();
				$q->in_queue = 1;
				$q->song_id = $song->id;
				$q->request_time =date('Y-m-d H:i:s');;
				$q->save();
			}
		}
		$this->getHelper('json')->sendJson(array(),true);
	}
	public function queuelistAction(){
		if($this->_jsonrequest){
                    
			$queue = new Application_Model_Queue();

			$q = $queue->getdb()->query(
				'SELECT q.id,s.title,a.name as name,g.name as genre 
				FROM  `queue` q
				JOIN song s on s.id = q.song_id
				LEFT JOIN artist a on a.id = s.artist_id
				LEFT JOIN genre g on g.id = s.genre_id
				WHERE `in_queue` = 1
				ORDER BY id asc
				limit 0, 50'
			);
			
			$this->getHelper('json')->sendJson( $q->fetchAll(), true);
		}
		echo 'Not today.';
		die();
	}
}