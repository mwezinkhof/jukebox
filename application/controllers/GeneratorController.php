<?php
include_once('../library/ext/simple_html_dom.php');
class GeneratorController extends Kaaz_Controller{
	
    public static $_menu_items = array('index'=>array('permission'=>11,'name'=>'Generate'));
	public function indexAction(){
	
	//	$db = Zend_Registry::get('dbAdapter');
	//	$r =$db->fetchAll("SELECT * FROM `user`");
		//var_dump($r);
		$x = new Kaaz_Tools_generator();
		
		/*
		 * Alle Tabellen met een ID zijn mogelijk!
		 * Dus:
		$models = $x->getdb()->fetchAll("	SELECT TABLE_NAME
											FROM INFORMATION_SCHEMA.COLUMNS
											WHERE COLUMN_NAME = 'id'
											AND TABLE_SCHEMA = 'test'");
		 */
		
		
		$x->setModelName('Queue');
		$x->setTable('queue');
		$x->generate();
	}
	public function countryAction(){
		///
            $list = file_get_html('http://nl.wikipedia.org/wiki/ISO_3166-1');
            $tbl = ($list->find('table.wikitable',0));
            $countries = $tbl->children();
            unset($countries[0]);
            foreach($countries as $country){
                $c = new Application_Model_country($country->children(2)->plaintext,'code_three');
                $c->name = str_replace('&#160;','',trim($country->children(0)->plaintext));
                $c->code_two = $country->children(1)->plaintext;
                $c->code_three = $country->children(2)->plaintext;
                $c->country_number = $country->children(3)->plaintext;
                $c->save();
            }
	}
}