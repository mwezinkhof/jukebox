<?php
require(realpath(dirname(__FILE__ )) .  '/baseCron.php');

$queue = new Application_Model_Queue(array('in_queue'=>1));

if($queue->id > 0){
	
	$song = new Application_Model_Song($queue->song_id);
	$queue->in_queue = 0;
	$queue->played_time = date('Y-m-d H:i:s');
	$queue->save();
	echo $song->file;
}
else{
	$song = new Application_Model_Song();
	$sq = $song->getdb()->query("SELECT file FROM `song` ORDER BY RAND() LIMIT 0,1")->fetch();
	echo $sq->file;
}