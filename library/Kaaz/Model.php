<?php
/**
 * Class Kaaz_model
 * Zeh originality
 * @category Model
 * @version 0.2
 *
 * All models with database connectivity should extend this one
 */
abstract class Kaaz_model {

    /**
     * Wheneter something changed in the values. <br/>
     * If there are no changes, there is no need to save.
     * @var boolean
     */
	protected $_dirty = false;
	
	protected $_relations = array();
	/*
	protected $_relations = array(
		'User' => array(
			'column'=> 'user_id',
			'refTable'=> 'User',
			'refColumn'=> 'id'
		)
	);
	 */
	/**
	 * Array for relation objects
	 * @var type Array of Kaaz_Model
	 */
	protected $_relation_objects = array();
    /**
     * Override for $_dirty
     * @see exportservice->$_dirty
     * @var boolean
     */
    protected $_can_dirty = false;

    /**
     * Name of the database table used.
     * @var String
     */
    protected $_tablename;

    /**
     * Used as the primary key in the table.
     * @var int
     */
    protected $_id;
    /**
     * Stores the database connection.
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_db;

    /**
     * Array of valid properties.<br/>
     * These values are also valid getters and setters.
     * @var Array<String>
     */
    protected $_valid_properties = array('id');

    /**
     * If an object is created with paramaters:<br/>
     * It uses the $this->getDb() to look in the table $_tablename<br/>
     * for the column $column with the value $id.<br/>
     * And sets the object's properties with the found results.
     *
     * @param int $id Optional
     * @param string $column Optional
     */
    public function __construct($id=0,$column='id') {
		foreach($this->_valid_properties as $prop){
			$this->$prop = '';
		}
        if(is_array($id)){
			$this->loadFromTableWithArray($id);
		}
		else{
			$this->loadFromTable($id,$column);
		}
        $this->allowDirty();
    }
	/**
	 * 
	 * @param String $name relationName as defined in @see $this->_relations;
	 */
	public function getRelation($name=false){
		if(key_exists($name, $this->_relations)){
			$className='Application_Model_'.$this->_relations[$name]['refTable'];
			
			if($this->_relation_objects[$name] instanceof $className){
				return $this->_relation_objects[$name];
			}
			try{
				$this->_relation_objects[$name] = new $className();
				$this->_relation_objects[$name]->find(array($this->_relations[$name]['refColumn']=> $this->_{$this->_relations[$name]['column']}));
				return $this->_relation_objects[$name];
			}
			catch(Exception $e){
				echo 'The relation "', $name, '" is not defined properly!<br/>';
				echo 'See Kaaz_Model::$_relations for more info.';
				var_dump($e);
				die();
			}
		}
	}
	
    /**
     * Returns the database connection
     * @return Zend_Db_Adapter_Abstract
     */
    public function getdb(){
        if(!($this->_db instanceof Zend_Db_Adapter_Abstract))
                $this->_db = Zend_Registry::get('dbAdapter');
        return $this->_db;
    }
    /**
     * Returns an array of all the valid properties and their values.
     * @return array
     */
    public function toArray(){
        $ret = array();
        foreach($this->_valid_properties as $prop){
                $ret[$prop] = $this->$prop;
        }
        return $ret;
    }
    public function getValidProperties(){
        return $this->_valid_properties;
    }

    /**
     * Setting this to true allows the dirty property to be set
     * @param boolean $allow
     */
    protected function allowDirty( $allow=true){
        $this->_can_dirty = (bool) $allow;
    }

    /**
     * Checks if the object is dirty
     * @return boolean
     */
    protected function isDirty(){
        return $this->_dirty;
    }
    /**
     * attempts to mark the object as dirty. Can force it to set.
     * @param boolean $force Optional: forces the object to be dirty regardless wheneter its allowed.
     */
    protected function setDirty($force=false){
        if($this->_can_dirty || $force){
            $this->_dirty = true;
        }
    }

    protected function _set_id($id){
        $this->_id = max(0,intval($id));
    }
    /**
     * @name Generic Getter
     * @example $obj->abc;<br/>
     * This would require 'protected $_abc' in the model.<br/>
     * Also requires 'abc' to be in the '$_valid_properties' array.<br/>
     * @param String $property
     * @return protected $property
     * @throws Exception When $property is not in the _valid_properties array
     */
    public function __get($property){
        if(in_array($property, $this->_valid_properties)){
            return $this->{'_'.$property};
        }
        else{
            throw new Exception('The Property "'.$property.'" does not exist in "'.get_class($this).'".');
        }
    }
    /**
     * @name Generic Setter
     *
     * @example $obj->abc = 3;<br/>
     * This would require a 'protected $_abc' variable to be set.<br/>
     * It would also require 'abc' to be added to the $_valid_properties array.<br/>
     * Optional: A method '_set_abc($val)' to manipulate the input.
     * @param string $property
     * @param mixed $value
     * @return mixed protected $property
     * @throws Exception When $property is not in the _valid_properties array
     */
    public function __set($property, $value) {

        if(in_array($property, $this->_valid_properties)){
            $old = $this->{'_'.$property};
            if(method_exists($this, '_set_'.$property)){
                call_user_func(array($this,'_set_'.$property),$value);
            }
            else{
                $this->{'_'.$property} = $value;
            }
            if($this->_can_dirty && $old != $this->{'_'.$property}){
                $this->_dirty = true;
                #echo 'Changed! val:'.$property.' Old{'.$this->{'_'.$property}.'} New{'.$value.'}';
            }
        }
        else{
            throw new Exception('The Property "'.$property.'" does not exist in "'.get_class($this).'".');
        }

    }

    /**
     * Saves the object to the database.
     * Only saves when data changed
     * @param type $saveRelations optional: Save the relation with other objects.
     */
    public function save($saveRelations=false) {
        if($this->_dirty){
            $data = array();
            foreach($this->_valid_properties as $field){
                if($field != 'id'){
                    $data[$field] = $this->{$field};
                }
            }
            $this->saveToTable($data);
        }
    }
    /**
     * Performs a delete query
     * @param array $where
     * @param String $table optional: Uses $this->_tablename by default
     */
    public function delete(array $where,$table=false){
        $targetTable = ($table !== false) ? $table : $this->_tablename;
        $this->getdb()->delete($targetTable,$where);
    }
    /**
     * Performs a select query
     * @param array $where
     * @param array $orderBy
     * @param type $table
     * @return type
     */
    public function fetchAll(array $where=array(), array $orderBy=array(),$table=false){//,$assosiations=false){
        $targetTable = ($table !== false) ? $table : $this->_tablename;
        $query = $this->getdb()	->select()
                                ->from($targetTable);
        foreach($where as $con => $val){
            $query->where($con, $val);
        }
        foreach($orderBy as $field => $direction){
            $direction = (strtoupper($direction) == 'ASC') ? 'ASC' : 'DESC';
            if(in_array($field, $this->_valid_properties)){
                $query->order($field . ' '.$direction);
            }
        }
        return $this->getdb()->query($query)->fetchAll();
    }
    /**
     * performs a query returning 2 values<br/>
     * Designed for comboboxes
     *
     * @param array $where
     * @param type $column_1
     * @param type $column_2
     * @param type $table optional: defaults to the $this->_tablename
     * @return type
     */
    public function fetchPairs(array $where=array(),$column_1='id',$column_2='name',$table=false){
        $targetTable = ($table !== false) ? $table : $this->_tablename;
        $query = $this->getdb()->select()
                        ->from($targetTable, array($column_1, $column_2));
        foreach($where as $con =>$val){
                $query->where($con,$val);
        }
        return $this->getdb()->fetchPairs($query);
    }
	/**
	 * 
	 * @param type $value Either value or Array (column => value,col2=>val2)
	 * @param type $column Only used when Value is not an array defaults to the column ID
	 * @param type $table db table, defaults to the model's table
	 * @param array $orderBy 
	 * @return type
	 */
    public function find($value,$column='id',$table=false,array $orderBy = array()){
        $targetTable = ($table !== false) ? $table : $this->_tablename;

        $query = $this->getdb()->select()
                                ->from($targetTable);
		if(!is_array($value)){
		  $query->where($column . ' = ?', $value);
		}
		else{
			foreach($value as $column => $filterVal){
				$query->where($column . ' = ?', $filterVal);
			}
		}
        foreach($orderBy as $field => $direction){
            $direction = (strtoupper($direction) == 'ASC') ? 'ASC' : 'DESC';
            $query->order($field . ' '.$direction);
        }
        //$query->limit(1,0);
        return $this->getdb()->query($query)->fetch();
    }

    public function deleteMe(){
        if($this->_id > 0){
            return $this->delete(array('id = ?'=>$this->_id),$this->_tablename );
        }
        return false;
    }
	public function loadFromTableWithArray(array $whereClause){
		$r = $this->find($whereClause);
		if($r !== false){
			$this->setData($r);
			return true;
		}
		return false;
	}
    public function loadFromTable($value=0,$column='id'){
        if($column=='id'){
            $value = max(0,intval($value));
            if($value == 0){
                return false;
            }
        }
        $r = $this->find($value,$column);
        if($r !== false){
            $this->setData($r);
            return true;
        }
        return false;
    }

    protected function setData($data){
        foreach($data as $key => $value){
            if(in_array($key, $this->_valid_properties)){
                    $this->$key = $value;
            }
            else{
                throw new Exception('Trying to set the property "'.$key.'" in the class "'.get_class($this).'->SetData()".');
            }
        }
    }
    protected function saveToTable($data,$table=false){
        $targetTable = ($table !== false) ? $table : $this->_tablename;

        if ($this->_id == 0) {
            $this->getdb()->insert($targetTable,$data);
			$this->_id = $this->getdb()->lastInsertId();
        }
                else {
            $this->getdb()->update($targetTable,$data, array('id = ?' => $this->_id));
        }
    }
}