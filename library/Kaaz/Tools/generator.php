<?php
/**
 * Class to generate other classes (Classception) DERP
 * @version 0.1
 */
class Kaaz_Tools_generator {

	protected $_model_name, $_table_name, $_db;
	protected $_class_path;
	protected $_base_model;
	protected $_interface_prefix;
	protected $_version = '0.1';
	protected $_dbAdapter = 'dbAdapter';

	public function __construct($dbadapter = false) {
		$this->_base_model = 'Kaaz_Model';
		$this->_class_path = APPLICATION_PATH.'/models/';
		$this->_interface_prefix = 'i';
		if($dbadapter != false){
			$this->_dbAdapter = $dbadapter;
		}
	}

	public function setModelName($name){
		$this->_model_name = $name;
	}
	public function setTable($tbl){
		$this->_table_name = $tbl;
	}
	public function getdb(){
		if(!($this->_db instanceof Zend_Db_Adapter_Abstract))
			$this->_db = Zend_Registry::get($this->_dbAdapter);
			
		return $this->_db;
	}
	public function generate(){
		if($this->_model_name != '' && $this->_table_name != '' && $this->_class_path != ''){
			
			if(strlen($this->_interface_prefix) == 0){
				$this->_interface_prefix = 'i';
			}

			$file_handler= fopen($this->_class_path.'object/'.$this->_model_name .'.php', 'w');
			$result = fwrite($file_handler, $this->generateObjectModel());
			fclose($file_handler);

			if(!file_exists($this->_class_path.$this->_model_name.'.php')){

				$file_handler= fopen($this->_class_path.$this->_model_name.'.php', 'w');
				$result = fwrite($file_handler, $this->generateModel());
				fclose($file_handler);
			}

			return true;
		}
		return false;
	}
	public function getColumninfo($tbl){
		$db_config = $this->getdb()->getConfig();

		return $this->getdb()->fetchAll("	SELECT *
											FROM INFORMATION_SCHEMA.COLUMNS
											WHERE table_name = '".$tbl."'
											AND TABLE_SCHEMA = '".$db_config['dbname']."'");
	}
	protected function generateObjectModel(){
		$protected_vars = array();
		$functions=array();
		$protected_vars[]=array(	'name'=>'tablename',
									'value'=>$this->_table_name,
									'comment'=> 'The database table used');
									
		foreach( $this->getColumninfo($this->_table_name) as $column){
			
			if($column->COLUMN_NAME == 'id'){
				continue;
			}
			$protected_vars[] = array(	'name'=>$column->COLUMN_NAME,
										'comment'=>$column->COLUMN_COMMENT,
										'datatype'=>$column->COLUMN_TYPE);

			$functions[] = array(	'name'=>'set_'.$column->COLUMN_NAME,
									'parameters'=>array('$val'),
									'codelines'=>$this->_setterFunction($column->COLUMN_NAME,$column->COLUMN_TYPE),
									'type'=>'protected');
		}
		$commentLines=array('Class object_'.$this->_model_name,
					'@Author Class: `'.  ucfirst(get_class($this)). '` Version: '. $this->_version,
					'',
					'This is a generated class',
					'',
					'Generation Details:',
					'Based on the database table `'.$this->_table_name.'`.',
					'Validation rules should be modified in the database.',
					'Generated on ' . date('Y-m-d @ H:i')
					);

		$model = $this->generateTemplate('abstract class','Application_Model_Object_'.$this->_model_name,$commentLines,$protected_vars,$functions,$this->_base_model);
		return $model;
	}

	protected function generateModel(){
		$commentLines = array();
		$protected_vars = array();
		$valid_props = array();
		$functions = array();
		foreach($this->getColumninfo($this->_table_name) as $column){
			$valid_props[] = $column->COLUMN_NAME;
		}
		$protected_vars[] = array(	'name'=>'valid_properties',
									'value'=>$valid_props,
									'comment'=>'These Values are used in the table `'.$this->_table_name.'`',
									'datatype'=>'Array');

		$commentLines=array('Class object_'.$this->_model_name,
			'@Author Class: `'.  ucfirst(get_class($this)). '` Version: '. $this->_version,
			'',
			'This is a generated class',
			'',
			'Generation Details:',
			'Based on the database table `'.$this->_table_name.'`.',
			'Validation rules should be modified in the database.',
			'Generated on ' . date('Y-m-d @ H:i')
			);
		$functions[] = array(	'name'=>'__constructor',
								'parameters'=>array('$id=0','$column=\'id\''),
								'codelines'=>array(	'parent::__construct($id,$column)',
													'//Code'),
								'type'=>'public');
		$functions[] = array(	'name'=>'save',
								'parameters'=>array('$saveRelations=false'),
								'codelines'=>array(	'parent::save()',
													'//custom save'),
								'type'=>'public');
		return $this->generateTemplate('class','Application_Model_'.$this->_model_name,$commentLines,$protected_vars,$functions,'Application_Model_Object_'.$this->_model_name);
	}


	protected function generateTemplate($type='class',$classname='stub',array $classComment=array(),array $protectedVars=array(),array $functions=array(),$extends=false,$implements=false){


		$template = "<?php\n";

		//class comment

		$template .=$this->_addComment($classComment,0);

		//class name
		$template .= $type." ".$classname;
		if($extends!=false){
			$template.= " extends " . $extends;
		}
		if($implements != false){
			$template .= " implements " .$implements;
		}
		$template .=" {\n\n";

		//protected vars
		if($type != 'interface'){

			foreach($protectedVars as $pvar){
				if($pvar['name'] == 'id'){
					continue;
				}
				if(isset($pvar['comment'])){
					$commentLines = array();
					if($pvar['comment'] != ''){
						$commentLines[] = $pvar['comment'];
						$commentLines[] = '';
					}
					if(isset($pvar['datatype'])){
						$commentLines[] = '@var '.$pvar['datatype'];
					}
					else{
						$commentLines[] = '@var String';
					}
					$template .=$this->_addComment($commentLines);
				}

				$template .= "\tprotected ".'$_'.$pvar['name'];
				if(isset($pvar['value'])){
					$template .= " = ".$this->_printValue($pvar['value']);
				}
				$template .=";\n\n";
			}
			foreach($functions as $func){
				if(isset($func['comment'])){
					$template .= $this->_addComment($func['comment']);
				}
				$template .= $this->_addFunction($func['name'], $func['parameters'], $func['codelines'], $func['type']);
			}
		}
		//closing tag
		$template .= '}';

		return $template;
	}
	protected function _setterFunction($varname,$type){
		$ret = array();
		if(substr($type, 0, 7)=="varchar"){

			$ret[] = '$this->_'.$varname.' = substr($val,0, '.substr($type,8,-1).')';
		}
		else if(substr($type,0,4) == 'enum'){

			$enums = explode(',', strtolower(str_replace('\'','',substr($type,5,-1))));
			$enums[] = '';
			$ret[] = 'if(in_array(strtolower($val), '.$this->_printArray($enums).')){';
			$ret[] = $this->_intToTab(1).'$this->_'.$varname.' = $val';
			$ret[] = '}';
			$ret[] = 'else{';
			$ret[] = $this->_intToTab(1).'throw new Exception("Trying to set `".get_class($this)."->'.$varname.'` to ". $val.". (Invalid Enum type)")';
			$ret[] = '}';
		}
		else if(substr($type,0,3) == 'int'){
			$ret[] = '$this->_'.$varname.' = (int) $val';
		}
		else if($type == 'tinyint(1)'){
			$ret[] = '$this->_'.$varname.' = (bool) $val';
		}
		else{
			$ret[] = '$this->_'.$varname.' = $val';
		}
		return $ret;
	}
	protected function _printValue($value){
		if(is_array($value)){
			return $this->_printArray($value);
		}
		if(is_string($value)){
			return "'".$value."'";
		}
		return $value;
	}
	protected function _printArray(&$array,$keys=false){
		$sArray = 'array(';
		foreach($array as $key => $val){
			if($keys){
				$sArray .= $this->_printValue($key)." => ";
			}
			$sArray .= $this->_printValue($val).", ";
		}
		$sArray .=')';
		return $sArray;
	}
	protected function _addComment(array $comment_lines,$tabs=1){

		$comment = $this->_intToTab($tabs) . "/**\n";
		foreach($comment_lines as $line){

			$com = explode("\n",$line);
			foreach($com as $c){
				$comment .= $this->_intToTab($tabs) ." * ".$c."\n";
			}
		}
		$comment .= $this->_intToTab($tabs)." */\n";
		return $comment;
	}
	protected function _addFunction($name,$parameters=array(),array $codeLines=array(),$type='public'){

		if(!in_array($type,array('public','protected','private'))){
			$type = 'public';
		}
		$func_prefix = ($type == 'public') ?'' :'_';
		$func = $this->_intToTab(1) . $type . ' function '.$func_prefix .$name . "(".implode(', ',$parameters).") {\n";
		foreach($codeLines as $line){
			$closeLine = ';';
			if(in_array($line[strlen($line)-1],array('{','}'))){
				$closeLine = '';
			}
			$func .= $this->_intToTab(2).$line.$closeLine."\n";
		}
		$func .= $this->_intToTab(1)."}\n\n";
		return $func;
	}
	protected function _intToTab($count){
		return str_repeat("\t",$count);
	}
}