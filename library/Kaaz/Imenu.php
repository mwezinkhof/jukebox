<?php
interface Kaaz_IMenu{

	/**
	 * @Return Array menu links within the controller.
	 */
	public static function getMenuItems($permission_required);

}