
function ajaxRequest(targetUrl,packet,callbackSuccess,callbackFail){
	Ext.Ajax.request({
		url: base+'/'+targetUrl+'/json/true',
		success: function(response,opts){if(typeof callbackSuccess == 'function')callbackSuccess(response);},
		failure: function(response,opts){if(typeof callbackFail == 'function')callbackFail(response);},
		params: packet
	});
}
function makeUrl(display_name,target_url){
	return '<a href="'+base+target_url+'">'+display_name+'</a>';
}
function redirect(newLocation){
	window.location =base+'/'+newLocation;
}
function numToDays(num){
	var days = ['Maandag','Disdag','Woensdag','Donderdag','Vrijdag'];
	return days[num];
}
function iconize(icon){
	return base + '/images/icons/fugue/' + icon + '.png';
}
function submitOnEnter(field, event) {
	if (event.getKey() == event.ENTER) {
		field.up('form').getForm().submit();
	}
}

function post_to_url(path, params, method) {
    method = method || "post"; 
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}


var ajaxManager = (function() {
	
	var queue = [];
	var stopAfterQueue= true;
	var delay = 2000;
	return {
	delay: delay,
	addToQueue:  function(item) {
		queue.push(item);
	},
	removeFromQueue:  function(item) {
	
		if( $.inArray(item, queue) > -1 )
			queue.splice($.inArray(item, queue), 1);
	},
	start: function() {
		var self = this;

		if( queue.length ) {
			orgComplete = queue[0].complete;

			queue[0].complete = function() {
				if( typeof orgComplete === 'function' ) 
					orgComplete();
				queue.shift();
				setTimeout(function(){
					self.start.apply(self);
				}, self.delay);
				
			};   

			$.ajax(queue[0]);
		}
		else{

			if(!stopAfterQueue){
			
				console.log('Looking for work..');
				self.tid = 	setTimeout(function() {
								self.start.apply(self);
							}, 1000);
			}
			else
				console.log('All done!');
		}
	},
	reset:  function() {
		queue = [];
		clearTimeout(this.tid);
	},
	setDelay: function(newdelay){
		this.delay = newdelay;
		console.log('setting new delay!');
	}
	};
}());