Ext.onReady(function(){
Ext.create('Ext.data.Store', {
	fields: ['id', 'title', 'name', 'genre'],
	storeId: 'queue_list',
	proxy: {
		type: 'ajax',
		url : base+'/queue/queuelist/json/true',
		reader: {
			type: 'json'
		}
	}
});
Ext.create('Ext.data.Store', {
	fields: ['id', 'title', 'name', 'genre'],
	storeId: 'song_library',
	proxy: {
		type: 'ajax',
		url : base+'/song/list/json/true',
		reader: {
			type: 'json'
		}
	}
});


});